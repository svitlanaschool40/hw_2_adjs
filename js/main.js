const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];
function isValidBook(book) {
  return book.hasOwnProperty('author') && book.hasOwnProperty('name') && book.hasOwnProperty('price');
}

function createBookList() {
  const root = document.getElementById('root');
  const ul = document.createElement('ul');

  books.forEach(function(book) {
    if (isValidBook(book)) {
      const li = document.createElement('li');
      li.textContent = `${book.author}, ${book.name}, ${book.price || 'The price is unknown'}`;
      ul.appendChild(li);
    } else {
      console.error('Error: The object contains incomplete information');
    }
  });

  root.appendChild(ul);
}

createBookList();